﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    class Item
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal price { get; set; }
        public Category categoryfk { get; set; }
    }
}
