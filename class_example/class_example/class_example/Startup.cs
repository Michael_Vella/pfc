﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(class_example.Startup))]
namespace class_example
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
