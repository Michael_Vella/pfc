﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class PropertyModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string area { get; set; }
        public int price { get; set; }
        public string descripition { get; set; }
        public string imagepath { get; set; }
    }
}