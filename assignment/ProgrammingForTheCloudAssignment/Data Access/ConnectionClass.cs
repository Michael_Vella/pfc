﻿using Npgsql;
using System.Web.Configuration;

namespace Data_Access
{
    public class ConnectionClass
    {
        public NpgsqlConnection MyConnection { get; set; }
        public NpgsqlTransaction MyTransaction { get; set; }

        public ConnectionClass()
        {            
            string str = WebConfigurationManager.ConnectionStrings["postgresConnection"].ConnectionString;
            MyConnection = new NpgsqlConnection(str);
        }
    }
}