﻿using Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access
{
    public class PropertyRepo: ConnectionClass
    {
        public PropertyRepo() : base() { }

        /// <summary>
        /// Returns a list of properties
        /// </summary>
        /// <returns></returns>
        public IQueryable<PropertyModel> GetProperties() {
            string sql = "Select id, name, area, price, " +
                         "descripiton, imagepath" +
                         " from property ";

            NpgsqlCommand command = new NpgsqlCommand(sql, MyConnection);

            List<PropertyModel> properties = new List<PropertyModel>();
            using( NpgsqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read()) {
                    properties.Add(new PropertyModel()
                    {
                        id = Convert.ToInt16(reader["id"]),
                        name = reader.GetString(1),
                        area = reader.GetString(2),
                        price = reader.GetInt32(3),
                        descripition = reader.GetString(4),
                        imagepath = reader.GetString(5)
                    });
                }
            }
            return properties.AsQueryable();
        }


       public void AddItem(PropertyModel i)
       {
            string sql = "Insert into property (name,area, price, descripiton, imagepath) Values (@name, @area, @price, @descripiton, @imagepath)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", i.name);
            cmd.Parameters.AddWithValue("@area", i.area);
            cmd.Parameters.AddWithValue("@price", i.price);
            cmd.Parameters.AddWithValue("@descripition", i.descripition);
            cmd.Parameters.AddWithValue("@imagepath", i.imagepath);
            cmd.ExecuteNonQuery();
       }

        public void DeleteItem(int id)
        {
            string sql = "delete from property where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Transaction = MyTransaction; //to make the command participate in an opened transaction


            cmd.ExecuteNonQuery(); //executenonquery >>> Insert, Update, Delete
        }

        public void UpdateItem(PropertyModel current)
        {
            string sql = "Update property Set name = @name, area = @area," +
                         "price = @price, descripition = @descripition, imagepath = @imagepath where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", current.name);
            cmd.Parameters.AddWithValue("@area", current.area);
            cmd.Parameters.AddWithValue("@price", current.price);
            cmd.Parameters.AddWithValue("@descripition", current.descripition);
            cmd.Parameters.AddWithValue("@imagepath", current.imagepath);

            cmd.ExecuteNonQuery();
        }
    }
}
