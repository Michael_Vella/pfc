﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace Data_Access
{
    public class RedisRepository
    {
        ConnectionMultiplexer cacheManager;
        public RedisRepository()
        {
            cacheManager = ConnectionMultiplexer.Connect(
                "redis-14991.c1.us-east1-2.gce.cloud.redislabs.com:14991,password=Kf9m4fNaKcHxRMKAU1mFLKGuPgNJsorM");

        }

        public List<PropertyModel> LoadItemsFromCache()
        {
            var db = cacheManager.GetDatabase();
            string property = db.StringGet("property");
            if (string.IsNullOrEmpty(property) == true)
            {
                return null;
            }
            else
            {
                return JsonConvert.DeserializeObject<List<PropertyModel>>(property);
            }

        }

        public void SetItemsInCache(List<PropertyModel> property)
        {
            var db = cacheManager.GetDatabase();
            if (property != null)
                db.StringSet("property", JsonConvert.SerializeObject(property));
        }

        public bool HasItemsInCacheChanged(List<PropertyModel> property)
        {
            string digest1 = HashValue(JsonConvert.SerializeObject(property));
            var dataInCache = LoadItemsFromCache();
            if (dataInCache == null) return false;
            string digest2 = HashValue(JsonConvert.SerializeObject(dataInCache));
            return !(digest1 == digest2);
        }

        private string HashValue(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputAsBytes = Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(md5.ComputeHash(inputAsBytes));
        }

    }
}
