﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProgrammingForTheCloudAssignment.Startup))]
namespace ProgrammingForTheCloudAssignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
