namespace ProgrammingForTheCloudAssignment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class recent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PropertyModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        area = c.String(),
                        price = c.Int(nullable: false),
                        descripition = c.String(),
                        imagepath = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PropertyModels");
        }
    }
}
