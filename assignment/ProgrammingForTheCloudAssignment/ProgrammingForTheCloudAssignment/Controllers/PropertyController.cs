﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Data_Access;
using Google.Cloud.Storage.V1;
using Google.Cloud.Diagnostics.AspNet;
using ProgrammingForTheCloudAssignment.Models;

namespace ProgrammingForTheCloudAssignment.Controllers
{
    public class PropertyController : Controller
    {
        // GET: Items
        [Authorize]
        public ActionResult Index()
        {
            PropertyRepo propertyRepo = new PropertyRepo();
            List<PropertyModel> properties = new List<PropertyModel>();
            try
            {
                Logger.Log("loading items from cache....", Google.Cloud.Logging.Type.LogSeverity.Info);
                properties = new RedisRepository().LoadItemsFromCache();
            }
            catch (Exception ex)
            {
                Logger.Log("error occurred while loading errors from cache", Google.Cloud.Logging.Type.LogSeverity.Error);
                //log exception
                var logger = GoogleExceptionLogger.Create("progforcloud63at", "SWD63BPFTC", "1");
                logger.Log(ex);
                ViewBag.Error = "Error occurred. try again later";
            }
            finally
            {

            }
            return View(properties);
        }
        // GET: Property/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Property/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create( PropertyModel propertyModel, HttpPostedFileBase file)
        {
            PropertyRepo propertyRepo = new PropertyRepo();
            try
            {
                if (propertyRepo.MyConnection.State == System.Data.ConnectionState.Closed)
                    propertyRepo.MyConnection.Open();

                //need to upload the file somewhere on the cloud storage

                var client = StorageClient.Create();
                var imageName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                var uploadObject = client.UploadObject("programmingforthecloud", imageName.ToString(), file.ContentType, file.InputStream,
                   new UploadObjectOptions() { PredefinedAcl = PredefinedObjectAcl.ProjectPrivate });

                propertyModel.imagepath = imageName; //uploadObject.MediaLink
                propertyRepo.AddItem(propertyModel);

                ViewBag.Success = "Property was added";
            }
            catch (Exception ex)
            {
                //log exception
               ViewBag(ex.ToString());
            }
            finally
            {
                if (propertyRepo.MyConnection.State == System.Data.ConnectionState.Open)
                    propertyRepo.MyConnection.Close();
            }
            return View();
        }

        // GET: Property/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Property/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Property/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Property/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
